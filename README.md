# Development environment

## Prerequisites
`docker` and `docker-compose` should be installed.

## Setup
Run `make clone` and the repo will be cloned.
After that the following environmental variables should be added
```
DATABASE_URL=
MAILER_FROM_ADDRESS=
MAILER_USERNAME=
MAILER_PASSWORD=
```
Run `make setup-db`

## Run the development server
`make server-start`

## Stop the development server
`make server-stop`
