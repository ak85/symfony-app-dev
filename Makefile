SHELL='/bin/bash'

create-project:
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /project_directory \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/..":/project_directory \
		composer create-project symfony/website-skeleton symfony-app 4.4.*

clone:
	git clone git@bitbucket.org:ak85/symfony-app.git ../symfony-app

setup-db:
	make console COMMAND=doctrine:migrations:migrate
	make console COMMAND=app:companies-import

server-start:
	make server-stop
	@ USERID=$(shell id -u) GROUPID=$(shell id -g) docker-compose up --build --detach

server-stop:
	@ USERID=$(shell id -u) GROUPID=$(shell id -g) docker-compose down

console-build:
	@docker build \
		--tag symfony-app-console \
		services/console

console:
	make console-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--env COMPOSER_CACHE_DIR=/vendor-cache \
		--volume "$(PWD)/vendor-cache":/vendor-cache \
		symfony-app-console bin/console ${COMMAND}

composer-build:
	@docker build \
		--tag symfony-app-composer \
		services/composer

composer-require-dev:
	make composer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--env COMPOSER_CACHE_DIR=/vendor-cache \
		--volume "$(PWD)/vendor-cache":/vendor-cache \
		symfony-app-composer composer require --dev

composer-install-dev:
	make composer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--env COMPOSER_CACHE_DIR=/vendor-cache \
		--volume "$(PWD)/vendor-cache":/vendor-cache \
		symfony-app-composer composer install

composer-install:
	make composer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--env COMPOSER_CACHE_DIR=/vendor-cache \
		--volume "$(PWD)/vendor-cache":/vendor-cache \
		symfony-app-composer composer install --no-dev

composer-require:
	make composer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--env COMPOSER_CACHE_DIR=/vendor-cache \
		--volume "$(PWD)/vendor-cache":/vendor-cache \
		symfony-app-composer composer require

composer:
	make composer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--env COMPOSER_CACHE_DIR=/vendor-cache \
		--volume "$(PWD)/vendor-cache":/vendor-cache \
		symfony-app-composer composer

php-cs-fixer-build:
	@docker build \
		--tag symfony-app-php-cs-fixer \
		services/php-cs-fixer

php-cs-fixer:
	make php-cs-fixer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		--volume "$(PWD)/services/php-cs-fixer/.php_cs.dist":/.php_cs.dist \
		symfony-app-php-cs-fixer php-cs-fixer fix --config=/.php_cs.dist --using-cache=false -vvv

phpunit:
	make composer-build
	@docker run \
		--rm \
		--tty \
		--interactive \
		--workdir /app \
		--user $(shell id -u):$(shell id -g) \
		--volume "$(PWD)/../symfony-app":/app \
		symfony-app-composer bin/phpunit
